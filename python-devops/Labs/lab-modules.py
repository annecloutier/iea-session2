# Lab: Modules

#     1. Create your own module, mymodule.py (or any name you choose) and
#     2. Import it from IDLE or the Python shell using both from and import syntax
#     3. Be sure you are understand how to access variables/data from your imported modules
#        and the difference between from mymodule and import mymodule
#     4. Take your calculate.py program and split it into two files:
#         a. A module which contains the calculate function, and
#         b. A main program which imports the calculate module

# https://www.programiz.com/python-programming/modules
