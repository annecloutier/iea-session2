# Lab: Exceptions_2

#     extend your calculator to allow 'log' as an operator
#         the second argument is the base, i.e,. calculate(49.0, 7, 'log') = log7(49.0) = 2.0
#         remember that logb(x) = loga(x)/loga(b))
#     use a try/except/else block around your code that computes the log

import math


def calculate(num_1, num_2, operation):
    num_1 = float(num_1)
    num_2 = float(num_2)
    if operation == '+':
        result = num_1 + num_2
        print(result)
    elif operation == '-':
        result = num_1 - num_2
        print(result)
    elif operation == '-':
        result = num_1 - num_2
        print(result)
    elif operation == '*':
        result = num_1 * num_2
        print(result)
    elif operation == '%':
        try:
            result = num_1 % num_2
        except ZeroDivisionError:
            print('You are not smarter than a 5th grader!')
    elif operation == '/':
        try:
            result = num_1 / num_2
        except ZeroDivisionError:
            print('You are not smarter than a 5th grader..!')
            result = 0  # there is no assignment yet
    elif operation == '//':
        try:
            result = num_1 // num_2
        except ZeroDivisionError:
            print('You are not smarter than a 5th grader...!')
            result = 0  # there is no assignment yet
    elif operation == 'log':
        try:
            log_1 = math.log(num_1)
            log_2 = math.log(num_2)
        except ZeroDivisionError:
            print('You cannot take log base 0')
        except ValueError:
            print('Log of negative values are undefined.')
        else:
            result = calculate(log_1, log_2, '/')
        finally:
            print('Finally!')
    return result


calculate(4, 0, '/')
