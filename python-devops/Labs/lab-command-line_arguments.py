# Lab: Command-Line Arguments

# - turn your calculate() function into a standalone program
# - which takes 3 command line arguments and
# - invokes calculate() with those arguments

import math
import sys


def calculate(num_1, num_2, operation):
    num_1 = float(num_1)
    num_2 = float(num_2)
    if operation == '+':
        result = num_1 + num_2
        print(result)
    elif operation == '-':
        result = num_1 - num_2
        print(result)
    elif operation == '-':
        result = num_1 - num_2
        print(result)
    elif operation == '*':
        result = num_1 * num_2
        print(result)
    elif operation == '%':
        try:
            result = num_1 % num_2
        except ZeroDivisionError:
            print('You are not smarter than a 5th grader!')
    elif operation == '/':
        try:
            result = num_1 / num_2
        except ZeroDivisionError:
            print('You are not smarter than a 5th grader..!')
            result = 0  # WHY THE RESULT VALUE ?
    elif operation == '//':
        try:
            result = num_1 // num_2
        except ZeroDivisionError:
            print('You are not smarter than a 5th grader...!')
    elif operation == 'log':
        try:
            num_1 = math.log(num_1)
            num_2 = math.log(num_2)
        except ZeroDivisionError:
            print('You cannot take log base 0')
        except ValueError:
            print('Log of negative values are undefined.')
        else:
            result = calculate(num_1, num_2, '/')
        finally:
            print('Finally!')
    return result


for arg_input, arg in enumerate(sys.argv):
    print(f'arg {arg_input} is {arg}')

print('Program arguments', sys.argv)
result = calculate(sys.argv[1], sys.argv[2], sys.argv[3])
print('Here\'s your answer: ', result)
