#! /usr/bin/env python3
import requests
import sys

DARK_SKY_SECRET_KEY = '1d8c58ed1d54f96f939e706c788650f1'


def get_location():
    """ Returns the longitude and latitude for the location of this machine.

    Returns:
    str: longitude
    str: latitude
     """

    location = requests.get('https://ipvigilante.com/')
    data = location.json()

    latitude = data['data']['latitude']
    longitude = data['data']['longitude']
    city = data['data']['city_name']
    state = data['data']['subdivision_1_name']
    city_state = f'{city}, {state} '

    # print(f'Longitude: {longitude} Latitude: {latitude}')

    return longitude, latitude, city_state


def get_current_forecast(longitude, latitude):
    """ Returns the current temperature at the specified location

    Parameters:
    longitude (str):
    latitude (str):

    Returns:
    float: temperature
    """
    """ clear
    temp
    high
    low
    humidity
    feels like"""

    api_call = requests.get(
        f'https://api.darksky.net/forecast/{DARK_SKY_SECRET_KEY}/{latitude},{longitude}')
    data = api_call.json()
    daily = data['daily']['data'][0]

    summary = data['currently']['summary']
    temperature = data['currently']['temperature']
    high_temp = daily['temperatureHigh']
    low_temp = daily['temperatureLow']
    humidity = data['currently']['humidity']
    feels_like = data['currently']['apparentTemperature']

    # print(temperature)

    return temperature, summary, high_temp, low_temp, humidity, feels_like


def get_time_machine(longitude, latitude, time):

    api_call = requests.get(
        f'https://api.darksky.net/forecast/{DARK_SKY_SECRET_KEY}/{latitude},{longitude},{time}')
    data = api_call.json()


def print_forecast(city_state, temp, summary, high_temp,
                   low_temp, humidity, feels_like):
    """ Prints the weather forecast given the specified temperature.
    Parameters:
    temp (float)
    """
    print(f"Today's forecast in {city_state}")
    print()
    print(summary)
    print()
    print(f'Temperature: {temp}')
    print(f'High: {high_temp}')
    print(f'Low: {low_temp}')
    print(f'Humidity: {humidity}')
    print(f'Feels like: {feels_like}')


if __name__ == "__main__":
    longitude, latitude, city_state = get_location()
    # Need to determine if its current or for a specifc time
    temp, summary, high_temp, low_temp, humidity, feels_like = get_current_forecast(
        longitude, latitude)
    print_forecast(
        city_state,
        temp,
        summary,
        high_temp,
        low_temp,
        humidity,
        feels_like)
