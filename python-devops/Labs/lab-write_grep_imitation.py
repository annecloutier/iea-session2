#! /usr/bin/env python3
# Lab: Write a Cheap Imitation of grep in Python

# - write a Python program which takes two command line arguments, a filename and a regex pattern
# - your program should act like grep in that it should search for the pattern in each line of the file
# - if the pattern matches a given line, print out the line
# - Challenge: make your search routines as reusable as possible
# - BONUS: Find a way to highlight the match on the line
# - EXTRA BONUS: Try allowing some of the behavior switches grep allows - line numbers, filename printing, etc.

import re
import sys


def fix_a_flat(filename, search_pattern):
    linenum = 0
    for lines in open(filename):
        linenum += 1
        if re.search(search_pattern, lines):
            print(
                'Line Number:',
                linenum,
                'contains',
                '"',
                search_pattern,
                '"',
                'line is',
                lines)
            return
        # elif re.search():
        #     print(f'Sorry, {search_pattern} was not found.')
        #     print('Pleae try a new search.')
        #     break
    return


for arg_input, arg in enumerate(sys.argv):
    filename = str(sys.argv[1])
    search_pattern = str(sys.argv[2])
    print(f'arg {arg_input} is {arg}')

# filename = str(sys.argv[1])
# search_pattern = str(sys.argv[2])
fix_a_flat(filename, search_pattern)

# run at bash prompt: ./lab-write_grep_imitation.py
# /home/acloutier/code/iea-session2/change_tire.txt brake

# try:
#     if len(sys.argv) < 2 or len(sys.argv) > 2:
#         print('Too few arguments')
#         if len(sys.argv) > 2:
#             print('Too many arguments')
# except IndexError:
#     print('you must provide 2 arguments, a filename and a regex search pattern.')
