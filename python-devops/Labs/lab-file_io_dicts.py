# Lab: File I/O

# write a Python program which prompts the user for a filename, then opens
# that file and writes the contents of the file to a new file, in reverse
# order, i.e.,


#     Original file       Reversed file
#     Line 1              Line 4
#     Line 2              Line 3
#     Line 3              Line 2
#     Line 4              Line 1

# tire_file = ''' How to Change Tires

#     Find a Safe Location. As soon as you realize you have a flat tire, do not abruptly brake or turn. ...
#     Turn on Your Hazard Lights. ...
#     Apply the Parking Brake. ...
#     Apply Wheel Wedges. ...
#     Remove the Hubcap or Wheel Cover. ...
#     Loosen the lug nuts. ...
#     Place the Jack Under the Vehicle. ...
#     Raise the Vehicle With the Jack.'''

# len(tire_file)

tire_file = open('/home/acloutier/code/iea-session2/change_tire.txt')
lines = tire_file.readlines()
print(lines)
