# Lab: dictionary_Roman Numerals

#     use a dict to translate Roman numerals into their Arabic equivalents
#     1. load the dict with Roman numerals M (1000), D (500), C (100), L (50), X (10), V (5), I (1)
#     2. read in a Roman numeral
#     3. print Arabic equivalent
#     4. try it with MCLX = 1000 + 100 + 50 + 10 = 1160
#
# BONUS: If you have time, deal with the case where a smaller number precedes a larger number, e.g., XC = 100 - 10 = 90, or MCM = 1000 + (1000-100) = 1900
#     MCMXCIX = 1999

rom_nums = {
    'M': 1000,
    'D': 500,
    'C': 100,
    'L': 50,
    'X': 10,
    'V': 5,
    'I': 1
}
rn_input = input('Enter a Roman Numeral to translate to a number: ')
for rn in rom_nums:
    #rn_input = input('Enter a Roman Numeral to translate to a number: ')
    rn_input = rom_nums.keys
    if rn_input == rom_nums.keys:
        print(rom_nums.values[rn_input])

# print(rom_nums)


#---------------------------------------------------------------------#
# roman = {
#     'M': 1000,
#     'D': 500,
#     'C': 100,
#     'L': 50,
#     'X': 10,
#     'V': 5,
#     'I': 1,
# }

# numeral = input('Enter a Roman numeral: ')
# arabic_list = []

# for digit in numeral:
#     if digit not in roman:
#         print('invalid Roman numeral')
#         break
#     arabic_list.append(roman[digit])

# for index in range(0, len(arabic_list) - 1):
#     if arabic_list[index] < arabic_list[index + 1]:
#         arabic_list[index] = -arabic_list[index]
#     sum = 0

# print(arabic_list)
# print(sum(arabic_list))
