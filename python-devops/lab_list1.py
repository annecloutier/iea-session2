list1 = []
list2 = []
lists = [list1, list2]

while True:
    cmd = input('[A]dd, [R]emove, Re[V]erse, [D]isplay, [E]qual, or E[X]it?')
    if cmd in ['A', 'R', 'V']:
        which = int(input('List 1 or List 2?')) - 1
    if cmd == 'A':
        val = input('Value to add:')
        lists[which].append(val)
    elif cmd == 'R':
        val_type = input('Remove by [I]ndex or [V]alue?')
        val = input('Value to remove:')
        if val_type == 'I':
            lists[which].pop(int(val))
        else:
            lists[which].remove(val)
    elif cmd == 'V':
        lists[which].reverse()
    elif cmd == 'D':
        print('List 1: ', list1)
        print('List 2: ', list2)
    elif cmd == 'E':
        if sorted(list1) == sorted(list2):
            print('List contents are equal')
        else:
            print('List contents are not equal')
    elif cmd == 'X':
        break
    else:
        print('Unrecognized command')
