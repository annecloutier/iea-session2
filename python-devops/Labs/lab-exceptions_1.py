# Lab: Exceptions

# modify your calculate function to catch the ZeroDivisionError exception and print an informative message if the user tries to divide by zero, e.g.,
# >>> calculate(4, 2, '/')
# 2
# >>> calculate(4, 0, '/')
# You cannot divide by zero!
# 0

def calculate(num_1, num_2, operation):
    num_1 = float(num_1)
    num_2 = float(num_2)
    if operation == '+':
        result = num_1 + num_2
        print(result)
    elif operation == '-':
        result = num_1 - num_2
        print(result)
    elif operation == '-':
        result = num_1 - num_2
        print(result)
    elif operation == '*':
        result = num_1 * num_2
        print(result)
    elif operation == '%':
        try:
            result = num_1 % num_2
        except ZeroDivisionError:
            print('You are not smarter than a 5th grader!')
    elif operation == '/':
        try:
            result = num_1 / num_2
        except ZeroDivisionError:
            print('You are not smarter than a 5th grader..!')
    elif operation == '//':
        try:
            result = num_1 // num_2
        except ZeroDivisionError:
            print('You are not smarter than a 5th grader...!')
    return


calculate(3, 0, '//')
