# Lab: Lists_3

#     Write a Python program to maintain a list
#         Read input until the user enters 'quit'
#         Words that the user enters should be added to the list
#         If a word begins with '-' (e.g., '-foo') it should be removed from the list
#         If the user enters only a '-', the list should be reversed
#         After each operation, print the list
#         Extras:
#             If user enters more than one word (e.g, foo bar), add "foo" and "bar" to the list, rather than "foo bar"
# Same for "-", i.e., -foo bar would remove "foo" and "bar" from the list

word_list = []

while True:
    words = input('Add an item to the shopping list: ')
    if words != 'quit':
        word_list.append(words)
        print(word_list)
    else:
        print('Final Shopping List: ', word_list)
        break
