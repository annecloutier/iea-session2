# Lab: List Comprehensions

#     Start with Cartesian product example (colors x sizes of t-shirts) and add a third list, sleeves = ['short', 'long']
#       then write a new listcomp which generates the Cartesian product colors x sizes x sleeves.
#       tshirts should look like this:


#      [['black', 'S', 'short'],
#       ['black', 'S', 'long'],
#       ['black', 'M', 'short'],
#       ['black', 'M', 'long'],
#       ['black', 'L', 'short'],
#       ['black', 'L', 'long'],
#       ['white', 'S', 'short'],
#       ['white', 'S', 'long'],
#       ['white', 'M', 'short'],
#       ['white', 'M', 'long'],
#       ['white', 'L', 'short'],
#       ['white', 'L', 'long']]


import math
colors = ['red', 'white', 'blue']
sizes = ['S', 'M', 'L']
sleeve_lengths = ['sleeveless', 'longsleeve', 'shortsleeve']
shirt_options = [[color, size, sleeve_length] for color in colors
                 for size in sizes
                 for sleeve_length in sleeve_lengths]

shirt_options

# Use a list comprehension to create a list of the squares of the integers
# from 1 to 25 (i.e, 1, 4, 9, 16, …, 625)

squares_list = []
nums_list = list(range(1, 26))

for nums in nums_list:
    squares_list = squares_list.append(nums)
    squares = math.pow(nums, 2)
print(squares_list)
# print(squares)


# squares_list = [num for nums_list ** 2 in nums_list]


#     Given a list of words, create a second list which contains all the words from the first list which do not end with a vowel
# Use a list comprehension to create a list of the integers from 1 to 100
# which are not divisible by 5
