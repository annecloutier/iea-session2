# /usr/bin/env python3
# Lab: Calculator class

# Using the code from your previous calculate function in our earlier labs, try making your own Calculator type, which will mimic a handheld calculator.
# Your calculator should do the following:
#   1. Have a method for each of the operators you support (i.e. "add" for +, "subtract" for -, etc.).
#   2. Each of these methods should take two parameters for the operands.
#       A. Don't forget to bring along all the error handling we had before!
#   3. Keep track of a total, which should be set to the result of the previous calculation.
#   4. Make the second parameter of each method optional .
#       A. Optional - (provide a default)
#       B. If the second parameter is not supplied, perform the operation (add, multiply, etc.) with the current total and the single operand provided.
#   5. Create an "eval" method
#       A. that can take a string in the form "2 1 +", similar to what we supported before, and
#       B. call the appropriate method based on the operator.
#   6. Create a "clear" method to reset the total.
#   7. Create a "display" method to display the current total.

# BONUS:
#   - Keep track of the previous calculations as they are requested, and
#   - provide a new method, "history" which displays them in the order they were called.
#   - Your "clear" function should also clear history.

import math


class HandheldCalculator:
    def __init__(self):
        self.total = 0
        print('in __init__')
        print('Initial total: ', self.total)

    def display(self):
        print(self.total)

    # def add(self, num_1, num_2 = None):

    def add(self, num_1, num_2=0):
        num_1 = float(num_1)
        num_2 = float(num_2)
        if num_2 == 0:
            result = num_1 + self.total
        else:
            result = num_1 + num_2
        # print(display())
        self.total = result
        return result

    def subtract(self, num_1, num_2=0):
        num_1 = float(num_1)
        num_2 = float(num_2)
        if num_2 == 0:
            result = self.total - num_1
        else:
            result = num_1 - num_2
        self.total = result
        return result
# how to keep the total if 2 arguments are passed?

    def multiply(self, num_1, num_2=0):
        num_1 = float(num_1)
        num_2 = float(num_2)
        if num_2 == 0:
            result = num_1 * self.total
        else:
            result = num_1 * num_2
        self.total = result
        return result

    def divide(self, num_1, num_2=0):  # change the 0 to None so it doesn't automatically error?
        num_1 = float(num_1)
        num_2 = float(num_2)
# update to catch the ZeroDivisionError if num_1 or num_2 is zero
        if num_2 == 0:
            result = self.total / num_2
        if num_1 == 0:
            result = self.total / num_1
        else:
            result = num_1 / num_2
            try:
                result = num_1 / num_2
            except ZeroDivisionError:
                print('You are not smarter than a 5th grader..!')
                result = 0  # there is no assignment yet
        self.total = result
        return result


# ------------------------------------------------------------
#     def calculate(num_1, num_2, operation):
#         num_1 = float(num_1)
#         num_2 = float(num_2)
#         if operation == '+':
#             result = num_1 + num_2
#             print(result)
#         elif operation == '-':
#             result = num_1 - num_2
#             print(result)
#         elif operation == '-':
#             result = num_1 - num_2
#             print(result)
#         elif operation == '*':
#             result = num_1 * num_2
#             print(result)
#         elif operation == '/':
#             try:
#                 result = num_1 / num_2
#             except ZeroDivisionError:
#                 print('You are not smarter than a 5th grader..!')
#                 result = 0 # there is no assignment yet
#         return result

# calculate(4, 0, '/')
