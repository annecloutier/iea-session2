# Lab: Lists_1

#     write a Python program to maintain two lists and loop until the user wants to quit
#     your program should offer the user the following options:
#         add an item to list 1 or 2
#         remove an item from list 1 or 2 by value or index
#         reverse list 1 or list 2
#         display both lists
#
#         EXTRA: add an option to check if lists are equal, even if contents are not in the same order
#         (i.e., if list 1 contains ['fig, 'apple', 'pear'] and list 2 contains ['pear', 'fig, 'apple'], you should indicate they are the same)
# https://note.nkmk.me/en/python-reverse-reversed/

netflix_queue = []
prime_watchlist = []
netflix_option = ''
prime_option = ''
netflix_queue_desc = netflix_queue.reverse()
index_of_list = [0]

while True:
    modification_option = input('Would you like to "add" or "remove"? ')
    netflix_queue_desc = list(netflix_queue.reversed())

    if modification_option == 'add':
        which_list = input(
            "Which list would you like to modify, 'prime' or 'netflix'? ")
        if which_list == 'prime':
            prime_option = input(
                'Which Prime show, series, or movie would you like to add? ')
            prime_watchlist.append(prime_option)
            print('Prime Watchlist: ', prime_watchlist)
            # netflix_queue.reverse()
            print('Netflix Queue: ', netflix_queue_desc)

        elif which_list == 'netflix':
            netflix_option = input(
                'Which Netflix show, series, or movie would you like to add? ')
            netflix_queue.append(netflix_option)
            print('Netflix Queue: ', netflix_queue)
            print('Prime Watchlist: ', prime_watchlist)

    elif modification_option == 'remove':
        which_list = input(
            "Which list would you like to modify, 'prime' or 'netflix'? ")
        if which_list == 'prime':
            prime_option = input(
                'Which Prime show, series, or movie would you like to remove? ')
            prime_watchlist.remove(prime_option)
            print((f'Removing {prime_option} from your list'))
            print('Prime Watchlist: ', prime_watchlist)
        elif which_list == 'netflix':
            netflix_option = input(
                'Which Netflix show, series, or movie would you like to remove? ')
            netflix_queue.remove(netflix_option)
            print((f'Removing {netflix_option} from your list'))
            print('Netflix Watchlist: ', netflix_queue)

    elif which_list == '':
        print('Are you looking for Blockbuster?')
        continue

    elif which_list == 'hulu':
        print('You must be choose Netflix or Prime')
        continue

    elif which_list == 'quit':
        print('quitting the menu')
        break


###### FIRST WORKING PASS ######
# netflix_queue = []
# prime_watchlist = []
# netflix_option = ''
# prime_option = ''
# add_to_list = 0

# while True:
#     which_list = input('Which list would you like to modify? ')
#     if which_list == 'prime':
#         prime_option = input('Which Prime show, series, or movie would you like to add? ')
#         prime_watchlist.append(prime_option)
#         print('Prime Watchlist: ', prime_watchlist)
#     elif which_list == 'netflix':
#         netflix_option = input('Which Netflix show, series, or movie would you like to add? ')
#         netflix_queue.append(netflix_option)
#         print('Netflix Queue: ', netflix_queue)
#     elif which_list == 'quit' or which_list == 'Hulu':
#         print('quitting the menu')
#         break
#     elif which_list == '':
#         print('You must be looking for Block Buster')
#         break
