# Lab: Subprocess Automation

# * Explore the subprocess module and it's functions
# * Using one of the functions, write a script that takes a path name as a command-line argument
# * and runs the "du" command onit, capturing the output.
# * Have your script print only the summary information about the path
#     - (hint: you may need to look at du options)
