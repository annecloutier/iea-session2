# Lab: functions¶

#     Write a function `calculate` which is passed two operands and an operator and returns the calculated result, e.g., calculate(2, 4, '+') would return 6
#     Write a function which takes an integer as a parameter, and sums up its digits. If the resulting sum contains more than 1 digit, the function should sum the digits again, e.g., sumdigits(1235) should compute the sum of 1, 2, 3, and 5 (11), then compute the sum of 1 and 1, returning 2.
#     Write a function which takes a number as a parameter and returns a string version of the number with commas representing thousands, e.g., add_commas(12345) would return "12,345"
#     Write a function to demonstrate the Collatz Conjecture:
#         for integer n > 1
#             if n is even, then n = n // 2
#             if n is odd, then n = n * 3 + 1
#         ...will always converge to 1
#         (your function should take n and keep printing new value of n until n is 1)
# def user_input(num_1 = int(input('Enter the first number: ')), num_2 = int(input('Enter the second number: ')), operator = input('Please select an operator- +, -, /, *, %, // : ')):
#     return num_1, operator, num_2


# import operator

# def calculate(num_1 = int(input('Enter the first number: ')), num_2 = int(input('Enter the second number: ')), operation = input('Select an operator- +, -, /, *, %, // : ')):
#     if operation == '+':
#         result = num_1 + num_2
#         print(result)
#     elif operation == '-':
#         result = num_1 - num_2
#         print(result)
#     elif operation == '-':
#         result = num_1 - num_2
#         print(result)
#     elif operation == '*':
#         result = num_1 * num_2
#         print(result)
#     elif operation == '%':
#         result = num_1 % num_2
#         print(result)
#     elif operation == '/':
#         result = num_1 / num_2
#         print(result)
#     elif operation == '//':
#         result = num_1 // num_2
#         print(result)
#     return
# calculate()

def calculate(num_1, num_2, operation):
    num_1 = float(num_1)
    num_2 = float(num_2)
    if operation == '+':
        result = num_1 + num_2
        print(result)
    elif operation == '-':
        result = num_1 - num_2
        print(result)
    elif operation == '-':
        result = num_1 - num_2
        print(result)
    elif operation == '*':
        result = num_1 * num_2
        print(result)
    elif operation == '%':
        result = num_1 % num_2
        print(result)
    elif operation == '/':
        result = num_1 / num_2
        print(result)
    elif operation == '//':
        result = num_1 // num_2
        print(result)
    return


calculate(3, 1, '+')
